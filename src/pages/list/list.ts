
import { Component } from '@angular/core';
import { NavController,MenuController,AlertController} from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MidconnectorPage } from '../midconnector/midconnector'; 
import { CartdetailsPage } from '../cartdetails/cartdetails';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  
  public items = [];
  public brandItem = [];
  constructor(public navCtrl: NavController,
     public menuCtrl: MenuController,public alertCtrl: AlertController,public http: Http
   ) { 
     
      this.menuCtrl.enable(true, 'myMenu');
      this.fetchBrand();
   }

   fetchBrand(){

      var headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
      let options = new RequestOptions({ headers: headers });

      let postParams = {
        data: "",
        

      }

      this.http.post("http://infiget.com/api/api/categorylisting", JSON.stringify(postParams), options)
      .subscribe(data => {

        var response = JSON.parse(data['_body']); 
        console.log("response "+response.success);
        if(response.success == 1){
        //this.items = JSON.parse(todos); 
            console.log("RESULT == "+response.result[0].code)
            this.items = response.result; 
            for(var i=0; i<response.result.length;i++){
              for(var j =0; j<response.result[i].brands.length;j++){
                console.log(response.result[i].brands[j]);
                //brandItem = response.result[i].brands[j];
              }
              
            }
            //this.dataService.saveBrandItems();
        }else{

        }
      
      },error => {
        console.log(error);// Error getting the data
      });

   }
   midconnector(brands, subcategories){
   console.log("brand id "+brands.id);
    this.navCtrl.push(MidconnectorPage,{
      brands: brands,
      subcategories:subcategories
    })  
   }
  
  /*setFilteredItems() {
 
        this.items = this.dataService.filterItems(this.searchTerm);
 
    }*/

    CartPage(){
      //console.log("kjsdhfsg ",this.brands);
      this.navCtrl.push(CartdetailsPage, {})  

    }
}
