import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TabaccessoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabaccessories',
  templateUrl: 'tabaccessories.html',
})
export class TabaccessoriesPage {

	public product_details : any[];
	constructor(public navCtrl: NavController, public navParams: NavParams) {

		this.navParams = navParams;
		console.log(this.navParams); // returns NavParams {data: Object}
		this.product_details = this.navParams.data;

		console.log("fgdfgd ",this.product_details[0].brandid);
		console.log("sub cat id ",this.product_details[0]['subcats'][1].id);

	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabaccessoriesPage');
  }

}
