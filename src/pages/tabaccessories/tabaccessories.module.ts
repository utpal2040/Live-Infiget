import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabaccessoriesPage } from './tabaccessories';

@NgModule({
  declarations: [
    TabaccessoriesPage,
  ],
  imports: [
    IonicPageModule.forChild(TabaccessoriesPage),
  ],
})
export class TabaccessoriesPageModule {}
