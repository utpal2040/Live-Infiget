import { Component } from '@angular/core';
import { NavController,ToastController ,MenuController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import 'rxjs/add/operator/map';
 import {Observable} from 'rxjs/Rx';
import { ListPage } from '../list/list'; 
import { HomePage } from '../home/home';   



@Component({
  selector: 'page-splashscreen',
  templateUrl: 'splashscreen.html',
})
export class SplashscreenPage {

  constructor(public navCtrl: NavController,private storage:Storage,public toastCtrl: ToastController, public menuCtrl: MenuController) {
    this.menuCtrl.enable(false, 'myMenu');
  }

  ionViewDidLoad() {

     this.storage.get('logindata').then(logindata=>{
     
     Observable.timer(30 * 60).subscribe(x => {
	        if(logindata !== null){
				this.navCtrl.push(ListPage, {})
	        }else{
               this.navCtrl.push(HomePage, {})
	        }

	     });  

	
	  });     
    }

}
