
import { Component } from '@angular/core';
import { NavController,MenuController,ToastController,AlertController  } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import {Storage} from '@ionic/storage';
import 'rxjs/add/operator/map';  
import { ListPage } from '../list/list';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-distributorcode',
  templateUrl: 'distributorcode.html',
})
export class DistributorcodePage {

   public unique_code: string;
   public id_user: string;
    public retailer_id: string; 
   public id_branddetails: string;
   public branddetails = [];


 constructor(public navCtrl: NavController, public menuCtrl: MenuController, public http: Http,public toastCtrl: ToastController,public loadingCtrl: LoadingController ,private storage:Storage,public alertCtrl: AlertController){
   this.menuCtrl.enable(false, 'myMenu');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DistributorcodePage');
  }

  goBack() {
     this.navCtrl.push(ListPage, {})

  }

   
    checkDistributerCode(){
	  if(this.unique_code == null || this.unique_code == '' ) {
	      let toast = this.toastCtrl.create({
	      message: 'Please enter the Distributer Code',
	      duration: 3000
	      });
	      toast.present();   
      
	  }else if(this.id_branddetails == null || this.id_branddetails == ''){
	     let toast = this.toastCtrl.create({
	      message: 'Must Select a Brand',
	      duration: 3000
	      });
	      toast.present();   

	  }else{ 
			

		    this.storage.get('logindata').then(logindata=>{
             this.retailer_id = logindata.userdetails[0].retailer_id; 
             if(this.retailer_id !== '' || this.retailer_id !== null){
				let loader = this.loadingCtrl.create({
				content: "Please wait..."
				});
				loader.present();
				let headers = new Headers();
				headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
				let options = new RequestOptions({ headers: headers });
				
				let reqParam = {
				  retailer_id : this.retailer_id

				}
				
				this.http.post("http://infiget.com/api/api/connectionschecking", JSON.stringify(reqParam), options)
				.subscribe(data => {
					loader.dismiss();
					var response = JSON.parse(data['_body']);
                    if(response.success == 1){
						let confirm = this.alertCtrl.create({
						title: 'Do you want to change the connection ?',
						message: response.message,
						buttons: [
						{
						text: 'Cancel',
						handler: () => {
						  this.navCtrl.push(ListPage, {})
						}
						},
						{
						text: 'Ok',
						handler: () => {


							let headers = new Headers();
							headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
							let options = new RequestOptions({ headers: headers });

							let reqParam2 = {
							 retailer_id : this.retailer_id,
							 distributor_code: this.unique_code,
							 brand_id: this.id_branddetails
	                        }
			                this.http.post("http://infiget.com/api/api/connectionsupdate", JSON.stringify(reqParam2), options)
							.subscribe(data => {
								loader.dismiss();
								var response = JSON.parse(data['_body']);
			                    if(response.success == 1){
									let toast = this.toastCtrl.create({
									message: response.message,
									duration: 3000
									});
									toast.onDidDismiss(() => {
								      this.navCtrl.push(ListPage, {})
									});
									toast.present();
								}else{

								   let toast = this.toastCtrl.create({
									message: "You are successfully connected",
									duration: 3000
									});
									
									/*response.message*/
									toast.present();


								}	
								
							}); 
						

						}
						}
						]
						});
						confirm.present();	
                    }else{ 
                        let toast = this.toastCtrl.create({
						message: response.message,
						duration: 3000
						});
						toast.present();
						
                    }
				}); 

             }else{
				let toast = this.toastCtrl.create({
				message: 'Sorry!Please try after some time' ,
				duration: 3000
				});
				toast.present();

             }

		    });  
	  }	     
   }  

 



  brandbydistributorcode(){

    if(this.unique_code){
      if(this.unique_code.length === 6 || this.unique_code.length > 6){
		let loader = this.loadingCtrl.create({
		content: "Please wait..."
		});
		loader.present();
        let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
        let options = new RequestOptions({ headers: headers });
        let reqParam = {
          distributor_code : this.unique_code
        }
        this.http.post("http://infiget.com/api/api/brandbydistributorcode", JSON.stringify(reqParam), options)
        .subscribe(data => {
           loader.dismiss();
          var response = JSON.parse(data['_body']);
          console.log(response);
          if(response.success == 1){
            this.branddetails = response.branddetails;

            
          }
        });
      } else {
        console.log("Enter 6 digit");
      }
    } else {
      console.log("Enter distributor code");
    }
  }  


}
