import { Component } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {Storage} from '@ionic/storage';
import 'rxjs/add/operator/map';
import { NavController,MenuController,ToastController } from 'ionic-angular';
import { RetailerSignup } from '../retailersignup/retailersignup';
import { ListPage } from '../list/list';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  
  constructor(public navCtrl: NavController,
     public menuCtrl: MenuController,public http: Http,private storage:Storage
   ,public toastCtrl: ToastController  ) { 
     
      this.menuCtrl.enable(false, 'myMenu');
       
        
   }

   password: string;
   username: string;

  
  loginUser() {
    var password = this.password;
    var username = this.username;
   if(username == '' || password == '' || username == null || password == null  ){ 
      let toast = this.toastCtrl.create({
      message: 'Oppps! Please fill all the Fields',
      duration: 3000
      });
      toast.present();
    }else{
    var headers = new Headers();
    headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
    let options = new RequestOptions({ headers: headers });
    
    let postParams = {
      username: username,
      password: password
     
    }
    
    this.http.post("http://infiget.com/loginwebservice/login", JSON.stringify(postParams), options)
      .subscribe(data => {
       
      var response = JSON.parse(data['_body']); 
      if(response.success == 0){
       let toast = this.toastCtrl.create({
      message: response.msg ,
      duration: 3000
      });
      toast.present();

      }else{
          
          this.storage.set('logindata',response); 
          this.navCtrl.push(ListPage, {})
          
       }   

      
       },error => {
        console.log(error);// Error getting the data
      });

    }
} 

      
  newRegistration() {
      this.navCtrl.push(RetailerSignup, {
    })
  }

   underconstruction() {

      let toast = this.toastCtrl.create({
      message: 'This functionality is Under Construction',
      duration: 3000
      });
      toast.present();
  }

  welcome(){
   this.navCtrl.push(ListPage, {})

  }

  

}
