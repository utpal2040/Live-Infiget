import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';
import { ProductcatsubcatPage } from '../productcatsubcat/productcatsubcat'; 
import { CartdetailsPage } from '../cartdetails/cartdetails';

@Component({
  selector: 'page-midconnector',
  templateUrl: 'midconnector.html',
})
export class MidconnectorPage {

  public brands : any;
  public subcategories : any[];
  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public navParams: NavParams) {
    this.menuCtrl.enable(true, 'myMenu');

    console.log("id == "+this.navParams.get('subcategories'));  
    this.brands = this.navParams.get('brands').id;
    this.subcategories = this.navParams.get('subcategories');


  }

  productcatsubcat(){
  	console.log("kjsdhfsg ",this.brands);
    this.navCtrl.push(ProductcatsubcatPage, {
      brands: this.brands,  
      subcategories: this.subcategories
    })   

  }
  CartPage(){
      console.log("kjsdhfsg ",this.brands);
      this.navCtrl.push(CartdetailsPage, {})  

    }
}
