import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController} from 'ionic-angular';
import { LoadingController, ToastController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'; 
import { CartdetailsPage } from '../cartdetails/cartdetails';
import {Storage} from '@ionic/storage';

import * as $ from 'jquery'

@IonicPage()
@Component({
  selector: 'page-productdetails',
  templateUrl: 'productdetails.html',
})
export class ProductdetailsPage {

  name;
  fullimage;
  thumbimage;
  short_description;
  long_description;
  mrp;
  public productID : any;
  
  public cartBtnState:boolean = false;

  public cart : any[];
  public cart_list : any[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public menuCtrl: MenuController, public loadingCtrl: LoadingController, public http: Http, public toastCtrl: ToastController,private storage:Storage) {
  	this.menuCtrl.enable(true, 'myMenu');

    this.productID = this.navParams.get('product_id');
    //console.log('productID', this.productID);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductdetailsPage');

    let loader = this.loadingCtrl.create({
        content: "Please wait..."
      });
      loader.present();
      let headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
      let options = new RequestOptions({ headers: headers });

      let reqParam = {
          product_id: this.productID,
      };

      console.log("PARAMS -- "+ JSON.stringify(reqParam));

      this.http.post("http://infiget.com/api/api/productdetails", JSON.stringify(reqParam), options)
        .subscribe(data => {
            console.log(data['_body']);
	        var response = JSON.parse(data['_body']);
	        console.log(response);
	        var success = response.success;
        	if(success == 1){

				loader.dismiss();
				console.log("name "+response.result.name);
				this.name = response.result.name;
    			this.fullimage = response.result.fullimage;
    			this.thumbimage = response.result.thumbimage;
    			this.short_description = response.result.short_description;
    			this.long_description = response.result.long_description;
          this.mrp = response.result.mrp;
        	}else{
        		let toast = this.toastCtrl.create({
			        message: 'Sorry! Unable to Register' ,
			        duration: 3000
		        });
		        toast.present();
		        loader.dismiss();

        	}
        },error => {
          console.log(error);// Error getting the data
          let toast = this.toastCtrl.create({
          message: 'Sorry! Unable to Fetch Record... PLease try after some time' ,
          duration: 3000
          });
          toast.present();
          loader.dismiss();
        });

      }


  cartdetails(){
    this.navCtrl.push(CartdetailsPage, {})
  }
  
  addtocart(name,short_description,fullimage,mrp){
    
    if(this.cartBtnState === false){
      this.cartBtnState = true;
console.log("IF");
      $('.cart').text('Item Added');
    

      this.storage.get('cartdata').then(cartdata=>{
       console.log("name "+name);
        
        if(cartdata !== null){
           
          cartdata.push({ name: name, short_description: short_description, fullimage: fullimage, mrp: mrp, qty:1 });

          this.storage.set('cartdata',cartdata);
          console.log("cart IF",JSON.stringify(cartdata));
        
        }else{
          this.cart = [
            { name: name, short_description: short_description, fullimage: fullimage, mrp: mrp, qty:1 }
            
          ];
          
          /*this.cart_list = [
            { cart: this.cart }
            
          ];*/
          //this.cart_list.push(this.cart);
          //this.cart_list = [{this.cart]};
          //this.cart.push({ name: name, short_description: short_description, fullimage: fullimage, mrp: mrp });


          this.storage.set('cartdata',this.cart); 
          console.log("cart size",JSON.stringify(this.cart));
        }
    
      }); 
    }else{
      console.log("ELSE");
      let toast = this.toastCtrl.create({
      message: "Item Already Added" ,
      duration: 3000
      });
      toast.present();

    }
  }

  CartPage(){
    //console.log("kjsdhfsg ",this.brands);
    this.navCtrl.push(CartdetailsPage, {})  

  }
}
