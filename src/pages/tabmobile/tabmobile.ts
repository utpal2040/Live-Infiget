import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'; 
import { ProductdetailsPage } from '../productdetails/productdetails'

/**
 * Generated class for the TabmobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabmobile',
  templateUrl: 'tabmobile.html',
})
export class TabmobilePage {

  public brandid : any;
  public subcategoryid : any;

  public product_details : any[];
  public products : any[];

	rootNavCtrl: NavController;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,public http: Http, public toastCtrl: ToastController) {

    //console.log("URL",window.location.href);
  	this.rootNavCtrl = navParams.get('rootNavCtrl');
    //console.log("rootNavCtrl "+this.rootNavCtrl.name);


     this.navParams = navParams;
     //console.log(this.navParams); // returns NavParams {data: Object}
     this.product_details = this.navParams.data;

    this.brandid = this.product_details[0].brandid;
    this.subcategoryid = this.product_details[0]['subcats'][0].id;

    //console.log("fgdfgd ",this.product_details[0].brandid);
    //console.log("sub cat id ",this.product_details[0]['subcats'][0].id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabmobilePage');

    let loader = this.loadingCtrl.create({
        content: "Please wait..."
      });
      loader.present();
      let headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
      let options = new RequestOptions({ headers: headers });

      let reqParam = {
          brandid: this.brandid,
          subcategoryid: this.subcategoryid
      };

      console.log("PARAMS -- "+ JSON.stringify(reqParam));

      this.http.post("http://infiget.com/api/api/productlisting", JSON.stringify(reqParam), options)
        .subscribe(data => {
          console.log(data['_body']);
          var response = JSON.parse(data['_body']);
          console.log(response);
          var success = response.success;
          if(success == 1){

            loader.dismiss();
            //console.log("name "+response.result.name);
            this.products = response.result;

          }else{
            let toast = this.toastCtrl.create({
              message: 'Sorry! No Items Found...' ,
              duration: 3000
            });
            toast.present();
            loader.dismiss();

          }
        },error => {
          console.log(error);// Error getting the data
          let toast = this.toastCtrl.create({
            message: 'Sorry! Unable to Fetch Record... PLease try after some time' ,
            duration: 3000
          });
          toast.present();
          loader.dismiss();
        });

  }   

/* productdetails(productID) {
    console.log("productID ",productID.id);
    
  }*/

 productdetails(product, localNavCtrl: boolean = false) {
    //console.log("productID ",product.id);
    if (localNavCtrl) {
      this.navCtrl.push('ProductdetailsPage',{
        product_id: product.id,
        
      });
    } else {
      this.rootNavCtrl.push('ProductdetailsPage',{
        product_id: product.id,
        
      });
    }
  }

}
