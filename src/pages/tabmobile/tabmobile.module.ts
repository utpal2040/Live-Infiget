import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabmobilePage } from './tabmobile';

@NgModule({
  declarations: [
    TabmobilePage,
  ],
  imports: [
    IonicPageModule.forChild(TabmobilePage),
  ],
})
export class TabmobilePageModule {}
