import { NgModule, Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-cartdetails',
  templateUrl: 'cartdetails.html',
})
export class CartdetailsPage {

	public amount:number=1;
  public cartItem : any [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public menuCtrl: MenuController,private storage:Storage) {
  	this.menuCtrl.enable(true, 'myMenu');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartdetailsPage');

    this.getCartItems();
    
  }

  addAmount(cart){
    console.log(cart.qty);
    cart.qty = cart.qty+1;
  	//this.amount=this.amount+1;
  }

  removeAmount(cart){
    console.log(cart.qty);
    //cart.qty = cart.qty+1;
  	if (cart.qty === 0){
  		cart.qty=0;
  	}
  	else{
  		cart.qty=cart.qty-1;
  	}
  }
  
  getCartItems(){
   
    this.storage.get('cartdata').then(cartdata=>{
     console.log("name "+name);
      
      if(cartdata !== null){
         
        this.cartItem = cartdata;
        console.log("cart IF",JSON.stringify(this.cartItem));
      
      }else{
        
        //console.log("cart size",JSON.stringify(this.cart));
      }
  
    }); 
  }
}
