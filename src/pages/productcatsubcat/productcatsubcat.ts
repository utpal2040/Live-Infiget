import { Component } from '@angular/core';
import { NavController,MenuController,ToastController, NavParams} from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
//import { ProductdetailsPage } from '../productdetails/productdetails' 
import { CartdetailsPage } from '../cartdetails/cartdetails';

import {SuperTabsController} from "ionic2-super-tabs";

@Component({
  selector: 'page-productcatsubcat',
  templateUrl: 'productcatsubcat.html',
})
export class ProductcatsubcatPage {

  page0: any = 'TabmobilePage';
  page1: any = 'TabaccessoriesPage';
  page2: any = 'TabtabletPage';

  public tabIndex:Number = 0;

  public tabs : any[];

  showIcons: boolean = true;
  showTitles: boolean = true;
  pageTitle: string = 'Full Height';

  public brandid : any;
  public subcats : any;
  public sb_catid : any;

  public product_details : any[]; 
  public productDetails : any[];

  constructor(public navCtrl: NavController, private navParams: NavParams,public http: Http, public menuCtrl: MenuController,public toastCtrl: ToastController) {

  this.tabs = [
    { root: 'TabmobilePage' },
    { root: 'TabaccessoriesPage' },
    { root: 'TabtabletPage' },
  ];
    let tabIndex = this.navParams.get('tabIndex');
      if(tabIndex){
        this.tabIndex = tabIndex;

      }

      
    //console.log("URL -= ",this.navCtrl.getActive().name);
    //console.log("id == "+this.navParams.get('brands'));

    this.brandid = this.navParams.get('brands');
    this.subcats = this.navParams.get('subcategories');

    this.sb_catid = this.subcats[0].id;
    //alert(this.sb_catid);

  






    this.product_details = [
      {id: 1, brandid: this.brandid, tabs: this.tabs, subcats: this.subcats}
      
    ];

    this.productDetails = this.product_details[0].subcats;
   //console.log("product_details ",JSON.stringify(this.product_details[0].subcats));
   //console.log("productDetails ",JSON.stringify(this.product_details));
   this.menuCtrl.enable(true, 'myMenu');

   const type = navParams.get('type');
    switch (type) {
      case 'icons-only':
        this.showTitles = false;
        this.pageTitle += ' - Icons only';
        
        break;

      case 'titles-only':
        this.showIcons = false;
        this.pageTitle += ' - Titles only';
        
        break;
    }

  }
  
 midconnector(){
   let toast = this.toastCtrl.create({
      message: 'This functionality is Under Construction',
      duration: 3000
      });
      toast.present();

 }

 

onTabSelect(ev: any) {
    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
    //alert(ev.id);
  }

  
onTabSelect11(tab: { index: number; id: string; }) {
    console.log(`Selected tab: `, tab.id);
   // alert("jfhg")
  }

  CartPage(){
      //console.log("kjsdhfsg ",this.brands);
      this.navCtrl.push(CartdetailsPage, {})  

    }
}
