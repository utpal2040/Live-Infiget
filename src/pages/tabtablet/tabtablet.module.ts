import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabtabletPage } from './tabtablet';

@NgModule({
  declarations: [
    TabtabletPage,
  ],
  imports: [
    IonicPageModule.forChild(TabtabletPage),
  ],
})
export class TabtabletPageModule {}
