import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Data } from '../providers/data';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { RetailerSignup } from '../pages/retailersignup/retailersignup' 
import { SplashscreenPage } from '../pages/splashscreen/splashscreen' 
import { MidconnectorPage } from '../pages/midconnector/midconnector' 
import { DistributorcodePage } from '../pages/distributorcode/distributorcode'
import { CartdetailsPage } from '../pages/cartdetails/cartdetails'
import { ProductcatsubcatPage } from '../pages/productcatsubcat/productcatsubcat' 
//import { ProductdetailsPage } from '../pages/productdetails/productdetails'   

import { SuperTabsModule } from 'ionic2-super-tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RetailerSignup,
    SplashscreenPage,
    MidconnectorPage,
    DistributorcodePage,
    ProductcatsubcatPage,
    CartdetailsPage
    //ProductdetailsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot(),
    Ng2SearchPipeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RetailerSignup,
    SplashscreenPage,
    MidconnectorPage,
    DistributorcodePage,
    ProductcatsubcatPage,
    CartdetailsPage
    //ProductdetailsPage

  ],
  providers: [
    Data,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: LocationStrategy, useClass: PathLocationStrategy}
  ]
})
export class AppModule {}
